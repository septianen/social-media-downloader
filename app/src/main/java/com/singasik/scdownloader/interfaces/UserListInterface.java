package com.singasik.scdownloader.interfaces;


import com.singasik.scdownloader.model.story.TrayModel;

public interface UserListInterface {
    void userListClick(int position, TrayModel trayModel);
}
